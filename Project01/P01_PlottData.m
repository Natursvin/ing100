
% Original og filtrert lysverdi
subplot(3,1,1)
plot(Tid(1:k),Lys(1:k), 'r', Tid(1:k), NULLPUNKT(1:k), 'k',Tid(1:k), IIRLysFiltrert(1:k), 'b',Tid(1:k),FIRLysFiltrert(1:k),'g')  % plotter lyssignal,FIRsignal,IIRsignal
title('Lysverdi som funksjon av SAMPLE k (egentlig ikke tid)')
xlabel('Tid i sekunder')
ylabel('Lysverdi')
legend('Lys', 'IIRfilter', 'FIRfilter')

% Integrert lysverdi
subplot(3,1,2)
NULL = zeros(k); % Lager nullvektor til � plottes i figuren
plot(Tid(1:k), LysIntegrert(1:k), 'r', Tid(1:k), NULL(1:k), 'k') % plotter integrert lysverdi
xlabel('tid [sek]')
ylabel('Integrert lyssignal (omkring nullpunktet)')

% Derivasjons Graff
subplot(3,1,3)
NULL = zeros(k);
plot(Tid(1:k), DerivertLys(1:k), 'r', Tid(1:k), NULL(1:k), 'k') % plotter derivert lysverdi
%ylim([-50, 50])
xlabel('tid [sek]')
ylabel('Derivert lyssignal')


% tegn n� (viktig kommando)
drawnow

