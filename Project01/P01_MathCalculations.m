if(k>2)
    LysIntegrert(k) = 0;
    Ts(k-1) = Tid(k)-Tid(k-1);
    avvik(k) = Lys(k)-nullpunkt;                              % Regner ut avviket fra nullpunktet
    LysIntegrert(k) = LysIntegrert(k-1) + avvik(k-1)*Ts(k-1); % Regner ut det totale arealet av kurven
else
    LysIntegrert(k) = 0;
end

NULLPUNKT = zeros(k) + nullpunkt;
FIRLysFiltrert(k) = FIRFilter(Lys,k);                      % FIR Filter
IIRLysFiltrert(k) = IIRFilter(Lys,(k),IIRLysFiltrert);     % IIR Filter

DerivertLys(k) = NumDerivasjon(FIRLysFiltrert,(k),Tid);     % Deriverer Lys Signalet

    
     
