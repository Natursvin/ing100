function DataFiltrert = FIRFilter(Data,k)
        if(k <= 2)
            DataFiltrert = Data(k);
        else
            DataFiltrert = round(1/3*Data(k)) + round(1/3*Data(k-1)) + round(1/3*Data(k-2));
        end
    
end
