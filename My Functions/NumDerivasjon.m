function Derivert = NumDerivasjon(Data,k,Tid)
if(k > 3)
    Derivert = (Data(k)- Data(k-1))/(Tid(k)-Tid(k-1));
else 
    Derivert = 0;
end
end