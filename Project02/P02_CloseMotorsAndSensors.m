if online  
    motorB.Stop;      % Stopp motor B
    motorC.Stop;      % Stopp motor C
    clear joymex2     % Clear MEX-file to release joystick
    clear online k    % Clear online og k data (f�r eventuell lagring i *.mat)
    clear handle      % Clear handle to avoid warnings when online=0
    CloseSensor(SENSOR_1);     % Steng ned sensorer
    CloseSensor(SENSOR_2);     % Steng ned sensorer
    CloseSensor(SENSOR_3);     % Steng ned sensorer
    CloseSensor(SENSOR_4);     % Steng ned sensorer
    COM_CloseNXT(handle_NXT);      % Close NXT connection.
    
end
