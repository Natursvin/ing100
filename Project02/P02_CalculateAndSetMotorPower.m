% Motors
% beregner hvordan joystickdata skal brukes for 
% � f� motorene til � bevege seg

if online
    if JoyTurn<10&JoyTurn>-10
    % Beregner motorp�drag og lagrer i datavektor
    PowerB(k) = JoyForover(k);
    PowerC(k) = JoyForover(k);
    else
        PowerC(k) = JoyTurn(k)/150*JoyForover(k);
        PowerB(k) = -PowerC(k);
    end
   
    % Setter powerdata mot NXT
    motorB.Power = PowerB(k);
    motorB.SendToNXT();
    motorC.Power = PowerC(k);
    motorC.SendToNXT();
else
    pause(0.01) % simulerer NXT-Matlab kommunikasjon i online=0
end
