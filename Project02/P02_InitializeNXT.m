% Initialiserer NXT'en med sensorer og motorer.
% Initialiserer joysticken.

if online
    % Initialiser NXT
    COM_CloseNXT('all')
    clear joymex2
    handle_NXT = COM_OpenNXT();
    COM_SetDefaultNXT(handle_NXT);
    
    % Initialiser sensorer (legg til etterhvert)
    OpenAccelerator(SENSOR_1);
    OpenSound(SENSOR_2,'DB');
    OpenLight(SENSOR_3,'ACTIVE');
    OpenUltrasonic(SENSOR_4);


    % Initialiser motorer (legg til etterhvert)
    motorB = NXTMotor('B');
    motorC = NXTMotor('C');
    motorB.SmoothStart = 1;
    motorC.SmoothStart = 1;
        
    % Initialiser joystick
    joymex2('open',0);
    joystick      = joymex2('query',0);
    JoyMainSwitch = joystick.buttons(1);
end
