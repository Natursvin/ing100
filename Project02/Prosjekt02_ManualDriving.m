% Hovedfil

clear 
close all
online=0;
P02_InitializeNXT

while ~JoyMainSwitch
    P02_GetFirstMeasurement
    P02_GetNewMeasurement
    P02_CalculateAndSetMotorPower
end

P02_CloseMotorsAndSensors